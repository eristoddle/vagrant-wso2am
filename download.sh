#!/bin/sh

# Automated download of java
#
# Hopefully means you don't need to go online to download these files
#

# * Java JDK 7u4-b40
wget --no-cookies --no-check-certificate --header "Cookie: gpw_e24=http%3A%2F%2Fwww.oracle.com%2F; oraclelicense=accept-securebackup-cookie" "http://download.oracle.com/otn-pub/java/jdk/7u4-b20/jdk-7u4-linux-x64.tar.gz"

# * WSO2 API Manager (AM!)
wget http://product-dist.wso2.com/products/api-manager/1.7.0/wso2am-1.7.0.zip

# * WSO2 Business Activity Monitor (BAM!)
wget http://product-dist.wso2.com/products/business-activity-monitor/2.4.1/wso2bam-2.4.1.zip
