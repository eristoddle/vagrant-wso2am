# WSO2 API Manager QuickStart

The aim of this repository was to give you everything you need to configure a virtual
machine that works for the [Quick Start Guide][QS]. 

With that aim the repository contains the application software WSO2 API Manager, WSO2 Business Activity Monitor, and the Oracle JDK. These are under the files/ subdirectory.

There is also a puppet script that installs, configures, and starts all of the services above under the manifest/ subdirectory.

1. git clone git@bitbucket.org:timb7/vagrant-wso2am.git
2. cd vagrant-wso2am
3. vagrant up
4. Go the [Quick Start Guide][QS] Guide. Start at 'Creating users and roles'. Skip the
   section on 'Configuring statistics'.

[QS]: https://docs.wso2.com/display/AM170/Quick+Start+Guide

Some additional notes on the Quick Start. 

In the section Invoking the API, don't use the API Console "Try it out!". It doesn't work. Rather use curl or the Tools->REST Client.

```
http://localhost:8280/phoneverify/1.1.0/CheckPhoneNumber?PhoneNumber=18006785432&LicenseKey=0
```
With the Authorization header containing the generated key from when you subscribe:
```
Authorization: Bearer JeSXxZDDzBnccK3Z2x
```

Skip the whole **Configuring statistics** section the puppet scripts did this all for you.

In the section Viewing statistics section, the first time you go to the statistics view it my just show the *Sample* view as shown in the Quick Start guide. If you go login to you vagrant box and bounce the BAM service then try again this should then work.

```
timb@TIMB ~/Projects/vagrant-wso2am (master)
$  vagrant ssh
[vagrant@localhost ~]$ sudo service wso2bam stop
Stopping WSO2 Business Manager ...
[vagrant@localhost ~]$ sudo service wso2bam start
Starting WSO2 Business Manager ...
[vagrant@localhost ~]$
```

TODO: 

1. Including the software in the git repository was probably not the best idea. It's a bit big with them and takes a while to download. I would be better to include instructions to download into the files/ here in the README. I do have a download.sh script that tries to get around the signups that are required for each of these software.

2. The puppet scripts need to be split into modules and classes.

NOTES FROM USING

- To change endpoint timeouts, go to localhost:9443/carbon and edit the service bus configuration looking for the value endpoint->timeout under the chosen api.
- If you are connecting to another vagrant box as your source for an api, you will have to add the hostname of that box to the /etc/hosts file on this box.
- Current issues with credentials I think due to the port setup. Only admin:admin works to log in and works everywhere. You can create new users, but the credentials won't work. This also seems to be causing an issue where the first token generated for an application works, but regenerating doesn't. But the sandbox access tokens can be regenerated. And it seems for testing purposes, you can set the sandbox and production urls to the same url. For now, that is a work around to play with WSO2.
- Wso2 apps are in the opt folder and more configuration values are in each apps repository/conf folder
- There are logs in the repository/logs folder of each app